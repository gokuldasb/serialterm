# Serial Terminal Experiments
This toy project explores a bunch of concepts around Serial Terminal in Linux:

- Linux device files (`/dev/*`)
- Serial links (UART)
- ANSI/VT100 Terminal Protocol
- Shell

The aim is to create a narrative into how the text terminal works on Linux,
including the historical decisions that lead to the design. We will accumulate
technical and historical references along the way.

We also aim to demonstrate the concepts with make a python program that behaves
like a standalone computer controlling an ANSI terminal. The entire setup is
built virtually inside Linux.

## Documentation
- [Table of Contents](./toc.md)
- [Introduction](docs/intro.md)

## Requirements & Usage
1. Linux distribution
    - socat
    - [picocom](https://github.com/npat-efault/picocom)
2. Python 3.6 +
    - [Poetry](https://python-poetry.org/)

Poetry is not completely essential. It is however a great way to manage python
dependencies. You can choose to use a virtual environment or user installation
to manage dependencies if you don't prefer poetry. Refer pyproject.toml for the
dependencies. I am going to assume you use poetry.

### Setup Environment
1. Clone this repository and cd to it
2. Create poetry virtual environment:
```sh
poetry install
```
3. Activate the virtual environment:
```sh
poetry shell
```
4. Follow instructions in the documentation as you progress. The simplest
example is the [teaser](docs/teaser.md).

## License
Copyright (C) 2020 Gokul Das Balachandran

The entire contents of this source tree is under MIT permissive open source
license. You may use, modify or redistribute the contents under the terms of
this license. Refer [LICENSE.txt](./LICENSE.txt) at the root of this tree for
more details. Check
[choosealicense.com](https://choosealicense.com/licenses/mit/) for simplified
details of this and other licenses.
