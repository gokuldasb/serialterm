# Teaser
Let's start out exploration of the Unix terminal with a simple teaser demo.
First of all, make sure you have installed all the requirements mentioned in
[README](../README.md). 

Here is what we will do next:  
![The three terminals of teaser](imgs/simple.png)

Open 3 terminals in Linux. We will call them (unimaginatively) Terminal-1,
Terminal-2 and Terminal-3.

## Terminal 1: Socat
Execute the following command in Terminal 1:
```sh
socat -d -d pty,raw,echo=0 pty,raw,echo=0
```

This command creates a virtual **serial link** (a communication wire) with two
ends. The commands prints out a few lines that look like this:  
```
2020/02/22 20:42:41 socat[2447686] N PTY is /dev/pts/14
2020/02/22 20:42:41 socat[2447686] N PTY is /dev/pts/15
2020/02/22 20:42:41 socat[2447686] N starting data transfer loop with FDs [5,5] and [7,7]
```

Take note of the parts in the message that looks like `/dev/pts/14` and
`/dev/pts/15`. These are called **device files** in Linux and here, they
represent the end points of the virtual wire. We will look at details of what a
serial link and device files are, in later chapters.

## Terminal 2: Picocom
Execute this command in Terminal 2:
```sh
picocom -b 19200 -fn -d8 -yn -p1 /dev/pts/15
```

Your device file may be different. Choose one from the output of `socat`.
The command will print out a few messages like this:
```
picocom v3.1

port is        : /dev/pts/15
flowcontrol    : none
baudrate is    : 19200
parity is      : none
databits are   : 8
stopbits are   : 1
escape is      : C-a
local echo is  : no
noinit is      : no
noreset is     : no
hangup is      : no
nolock is      : no
send_cmd is    : sz -vv
receive_cmd is : rz -vv -E
imap is        :
omap is        :
emap is        : crcrlf,delbs,
logfile is     : none
initstring     : none
exit_after is  : not set
exit is        : no

Type [C-a] [C-h] to see available commands
Terminal ready
```

## Terminal 3: PySerial
Activate poetry virtual environment in Terminal 3 as mentioned in README. Then
execute the follow command in it:
```sh
python -m serialterm.serialterm.simple /dev/pts/14
```

Use the following command instead if this doesn't work for you for some reason,
or if you are not using poetry:
```sh
python serialterm/simple/__main__.py /dev/pts/14
```

Again, remember that your device file may be different. Substitute it with the
unused device file from the output of `socat`.

## The Result
At this point, our last command outputs some mundane information on Terminal 3.
But look at the end of Terminal 2 (picocom). A new line has appeared on it!:  
```
Hello, from the other world!
```

If you're wondering where it came from, take a look at file
[serialterm/simple/__main__.py](../serialterm/simple/__main__.py). This was a
message sent from the command on Terminal 3. We have achieved cross-terminal
communication! Try changing the message in the source, execute it and see for
yourself!

At this point, you may be wondering if there weren't any simpler ways to do
this. Of course, there are. But our aim is to build up knowledge about serial
terminals. We just created a virtual hardware (the python program) communicating
to a serial terminal emulator (picocom) over a virtual serial link.

The stack that does this gets more fascinating as we look deeper at its design
and the history behind it. That's what we will do next.

## Closing the programs
Remember this part. You will want to close the programs when you're done
exploring. Here is how we do it.

The python program (terminal 3) stops on its own in this module. You can just
close the terminal after that. It may be different for other python modules. The
method will be revealed as we progress.

Socat (terminal 1) can be stopped by sending a `<CTRL>C` to the terminal.
Picocom (terminal 2) can be stopped by sending a `<CTRL>a` followed by a
`<CTRL>x` to that terminal.

[Previous: Introduction](./intro.md)  
[TOC](../toc.md)  
[Next: Ancient History](./ancient.md)
