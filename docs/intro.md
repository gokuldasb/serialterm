# Introduction

## Why do we care?
The terminal system in Linux (Unix in general) is complex, but made of a stack
of a few simple pieces. There is also a long and rich history behind this
design. Text terminals and serial links may seem arcane in this day and age. But
they still persist, due to the simplicity of their design.

One domain where they are still common are in *embedded systems*. Tiny computers
that control machines without interacting with humans. These are innumerable -
far more than even mobile devices, PCs and notebooks combined. They lack the
resource to have a full graphical interface. Serial links are ubiquitous with
them due to their simplicity. Embedded system developers love it.

Full fledged graphical interfaces on the other hand are finicky and unreliable
even in modern interactive devices (mobile, PCs, notebooks etc). Why? For one,
they are way more complex than text terminals. Worse, many of them have
technical details that are closely guarded secrets. This make them really hard
for developers to get right. How many times have we seen a GUI crash? And where
do we end up? On a text terminal! That is where we recover from (if you care).
And what if we could debug a locked up computer from another one?

In addition, the parts that make up the terminal are simple, independant and
extremely useful. We can use each of them in isolation or in combination to
great effect.

## A word on accuracy
This narrative is initially based on my memory of reading several articles. As
you may suspect, there may be several inaccuracies or factual errors. The aim
is to constantly improve on that front, including collecting all the best
references together. Send me a merge request if you find any such mistakes or
have a good reference to add.

[TOC](../toc.md)  
[Next: Teaser](./teaser.md)  
