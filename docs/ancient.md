# Ancient History

**NOTE:** *This part is written from memory of reading history. This may contain
a lot of factual inaccuracies. It could use a lot of fact checking and addition
of external references.*

Our story begins a long time ago - when dinosaurs roamed the planet. Alright -
not that far back! Just after World War II (*exactly when?*). Computing power
was growing - powered by invention of transistors and later by Integrated
Circuits. Computers started spilling over from restrictions of military
to businesses and scientific institutions.

Enough has been said about computers of that era. Suffice to say that they were
massive and weak compared to our era's computers. They were big enough to fill
large rooms. Using them required a few professionals.

A programmer would use a device that looks like a typewriter to 'punch' their
programs on to 'punched cards'. A single program would be a stack of these cards
in a set order. The programmer would then hand over the cards to an 'operator'.
The operator would then load the stack of cards into a machine called a 'card
reader'. The card reader would read the program from the cards and store it
into a large reel of magnetic tape. The data required by the program would also
be similarly converted to tapes.

Finally, the tapes (program and data) then get loaded onto the big computer.
Its primitive operating system then loads the program into memory and leaves
the program to deal with the data. This could be to compile a program, to
process a payroll or to calculate rocket trajectory. No matter what that was,
the operator could only wait for the program to complete. It was possible to
interrupt the program in between - but nothing else. This was called 'batch
processing'. A single computer was so costly that it was shared by dozens of
users. But their jobs could only taken one after another - each one taking
hours to complete!

## The next era
One thing is common for computing in every era. Computing power grows at
exponential speed. Computers became really fast in the next two decades. They
remained massive and costly. They were still shared by dozens of users in an
institution. But their speed made something else possible.

Operating systems exploited their speed to run multiple programs almost in
parallel. These were not the true parallel computers we have today. They used
a trick. The OS would run each program for a short while - a few milliseconds -
before it is forcefully interrupted. The OS would then load another program
in its place and do the same again. The OS would cycle through all the programs
in this fashion. This cycling is so fast that the computers seemed to run all
the programs simultaneously to a human. It would have been an incredible sight
for professionals of that era.

Multics was one of the first operating systems to do this. OS designers were not
very happy with it though. They used the Multics experience to the famed UNIX
operating system - the grandmother of most operating systems today. UNIX
introduced several revolutionary ideas to operating system design - collectively
called the 'UNIX philosophy'. These ideas are so fresh that it guides OS design
even today.

OS designers of that era discovered a pleasant side effect to multi-processing
(running multiple programs in parallel). Real time interaction with multiple
users. A few of the programs loaded in parallel in a computer were dedicated to
interacting with a user - or even multiple users. The era of 'Time Sharing'
systems were born!

## A new user interaction paradigm
Remember the punched cards and tapes of the previous era? They were unwieldy
and costly. The computers could talk to multiple users in parallel, but the
user interface devices could not keep up. New user interface devices had to be
invented. But no! The designers instead borrowed a solution from elsewhere.
Let's look at where that came from.

Depending on your age, you may or may not have seen a mechanical typewriter.
The clunky and noisy precursor to word processors. It wasn't long before an
electronic version of the typewriter appeared. They had soft touch keyboards
and an attached dot-matrix printer. Someone had to invent a method to
electronically convey information from keyboard to printer. This included
encoding for each and every letter and for jumping to next line on the paper
(called a **line feed**) and for moving the print head to the beginning of
that line (called a **carriage return**). You would be familiar with carriage
return and line feed, if you've ever used a mechanical type writer. They were
things you would do manually on it.

Anyway, people didn't stop innovating there. Here is what they did:

Normal Electric Typewriter:  
![Normal Electric Typewriter](imgs/e-type.png)

Teletype:  
![Teletype](imgs/tele-type.png)

Of course, they took two electronic typewriters and cross connected their
keyboard and printer together. The innovation was that these two units were
often in different cities. This was called by many names - Teletype,
Teleprinter, Telex. Often just **TTY** for short.

There must have been so many problems with sending signals over that
distances. But, hey! They started in 1830s! And by 1920s, this system was
almost perfected. They started replacing the Morse telegraph of the previous
century. TTYs were already well in operation by the time computers born.

So, how did TTYs (teletypes) influence the infant computer technology?
Simple! Computer designers figured that they could use TTY units as user
interface devices. The computer just needed a port to connect the TTY to.
A few ports, infact - to support multiple users at a time. Each user could
then send commands in real time using the keyboard and the computer would
respond to them on the printer.

The connection between the TTY and the computer was called the **Serial link**.
This was so because characters and control codes were sent one bit at a time, or
'serially' over this link. Remember the TTYs and Serial links. Unix terminals
still carry their legacy - as we will see soon.

## Age of the computer terminal
Imagine sitting in front of an electronic typewriter to communicate with a
computer. Rolls and rolls of paper being printed out in response to your
commands! Your ability to use the computer would be limited by your company's
ability to buy printer sheets in bulk! It didn't take much longer before
people replaced printer with something else - a CRT (cathode ray tube) monitor -
a modification of the ubiquitous TV. The combination of a keyboard and monitor
was called a **console**.

How were these monitors different from a TV? Remember that the dot matrix
printer could print out characters from short codes sent over the serial link.
This was possible because the shape of the character corresponding to each code
was stored in the memory of the printer. The same modifications were needed to
the TV to make it a computer monitor.

But a monitor also had some differences with the printer. Display space was
limited on a monitor, unlike in a printer. However, the same display space could
be reused on a monitor. You could print anywhere on the screen if you could
somehow move the cursor over there. Traditional printers couldn't do this and
the control codes didn't have that facility.

The easiest solution would have been to throw out the old control codes and
create a new one. But when did engineers ever do that?? New control codes
signals were made based on the old ones. These messages were coded as regular
characters. But if the monitor recognized them, they would do what the message
says instead of displaying them. We will be exploring this in detail in our
demos.

The control messages were soon standardized by ANSI (American National Standards
Institute). However, the standard wasn't adopted widely. That was until DEC
created the VT100 terminal. It used the ANSI standard with some additions, that
later came to be called the VT100 protocol. The VT100 and its successors became
hugely popular, selling millions world wide. This made the VT100 protocol the
base of most of the terminals we have today.

[Previous: Teaser](./teaser.md)  
[TOC](../toc.md)  
