"""Simple program to send message over serial link"""

import argparse

# Following few lines accepts the serial link device file from the user
# as a command line argument
parser = argparse.ArgumentParser()
parser.add_argument("device")
args = parser.parse_args()

print("Serial device simulator")
print("-----------------------")
print(f"Argument: device:\t {args.device}")

import serial

# Open the serial link and send message through it
with serial.Serial(args.device, 19200) as serlink:
    print(f"Serial link name:\t {serlink.name}")
    print("NOTE: The REAL interface exits at the OTHER END of the WIRE!")
    serlink.write(b"Hello, from the other world!\r\n");
